import mysql.connector
import hashlib
import datetime

thedb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="database",
        database="thedatabase"

)

thecursor = thedb.cursor()

print("her")

def list_users():

    thecursor.execute("SELECT id FROM users;")
    result = [x[0] for x in thecursor.fetchall()]

    return result

def verify(id, pw):

    thecursor.execute("SELECT pw FROM users WHERE id = '" + id + "';")
    result = thecursor.fetchone()[0] == hashlib.sha256(pw.encode()).hexdigest()

    return result

def delete_user_from_db(id):

    thecursor.execute("DELETE FROM users WHERE id = '" + id + "';")
    thedb.commit()

    # when we delete a user FROM database USERS, we also need to delete all his or her notes data FROM database NOTES
    thecursor.execute("DELETE FROM notes WHERE user = '" + id + "';")
    thedb.commit()

    # when we delete a user FROM database USERS, we also need to
    # [1] delete all his or her images FROM image pool (done in app.py)
    # [2] delete all his or her images records FROM database IMAGES
    thecursor.execute("DELETE FROM images WHERE owner = '" + id + "';")
    thedb.commit()

def add_user(id, pw):


    thecursor.execute("INSERT INTO users values(%s, %s)", (id.upper(), hashlib.sha256(pw.encode()).hexdigest()))
    thedb.commit()


def read_note_from_db(id):

    command = "SELECT note_id, timestamp, note FROM notes WHERE user = '" + id.upper() + "';"
    thecursor.execute(command)
    result = thecursor.fetchall()

    return result

def match_user_id_with_note_id(note_id):
    # Given the note id, confirm if the current user is the owner of the note which is being operated.

    command = "SELECT user FROM notes WHERE note_id = '" + note_id + "';"
    thecursor.execute(command)
    result = thecursor.fetchone()[0]

    return result

def write_note_into_db(id, note_to_write):

    current_timestamp = str(datetime.datetime.now())
    thecursor.execute("INSERT INTO notes values(?, ?, ?, ?)", (id.upper(), current_timestamp, note_to_write, hashlib.sha1((id.upper() + current_timestamp).encode()).hexdigest()))

    thedb.commit()


def delete_note_from_db(note_id):

    command = "DELETE FROM notes WHERE note_id = '" + note_id + "';"
    thecursor.execute(command)

    thedb.commit()

def image_upload_record(uid, owner, image_name, timestamp):

    thecursor.execute("INSERT INTO images VALUES (?, ?, ?, ?)", (uid, owner, image_name, timestamp))

    thedb.commit()

def list_images_for_user(owner):

    command = "SELECT uid, timestamp, name FROM images WHERE owner = '{0}'".format(owner)
    thecursor.execute(command)
    result = thecursor.fetchall()

    return result

def match_user_id_with_image_uid(image_uid):
    # Given the note id, confirm if the current user is the owner of the note which is being operated.

    command = "SELECT owner FROM images WHERE uid = '" + image_uid + "';"
    thecursor.execute(command)
    result = thecursor.fetchone()[0]

    return result

def delete_image_from_db(image_uid):

    command = "DELETE FROM images WHERE uid = '" + image_uid + "';"
    thecursor.execute(command)

    thedb.commit()


#thecursor.execute("CREATE DATABASE thedatabase")
#thecursor.execute("CREATE TABLE users (id VARCHAR(30), pw VARCHAR(100))")
#thecursor.execute("CREATE TABLE notes (node_id int(8), timestamp VARCHAR(20), note VARCHAR(500), act VARCHAR(30))")
#thecursor.execute("CREATE TABLE images (image_id int(8), timestamp VARCHAR(20), image_name VARCHAR(40), act VARCHAR(30))")
#thecursor.execute("DROP TABLE users")
add_user('admin', 'admin')


thecursor.execute("SHOW TABLES")

for tb in thecursor:
    print(tb)

thecursor.execute("SELECT * FROM users")

for row in thecursor:
    print(row)

name = 'admin'
pw = 'admin'


if __name__ == "__main__":
    print(list_users())
